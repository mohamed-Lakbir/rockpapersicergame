package Game;

public class RockPaperSicerGame {
    private int playerOneWins=0;
    private int playerTwoWins=0;
    private int draw;
    private int rounds;

    public void startGame(Player playerOne, Player playerTwo,int rounds){
        for(int i=0;i<rounds;i++){
            rule(playerOne.playRockPaperSicer(),playerTwo.playRockPaperSicer()
            );
        }
        this.rounds=rounds;
        printGameStats();
    }

    public void printGameStats(){
        System.out.println("Spieler A gewinnt " + this.playerOneWins + " von "+ rounds + " Spielen");
        System.out.println("Spieler B gewinnt " + this.playerTwoWins + " von "+ rounds + " Spielen");
        System.out.println("Unentschieden: " + this.draw + " von "+ rounds + " Spielen");
    }

    public void rule(Sign playerOne, Sign playerTwo){
        switch (playerOne){
            case ROCK:
                if(playerTwo.equals(Sign.SICER)){
                    playerOneWins++;
                }
                if(playerTwo.equals(Sign.PAPER)){
                    playerTwoWins ++;
                }
                if(playerTwo.equals(Sign.ROCK)){
                    draw++;
                }
            break;
            case SICER:
                if(playerTwo.equals(Sign.PAPER)){
                    playerOneWins++;
                }
                if(playerTwo.equals(Sign.ROCK)){
                    playerTwoWins++;
                }
                if(playerTwo.equals(Sign.SICER)){
                    draw++;
                }
                break;
            case PAPER:
                if(playerTwo.equals(Sign.ROCK)){
                    playerOneWins++;
                }
                if(playerTwo.equals(Sign.SICER)){
                    playerTwoWins++;
                }
                if(playerTwo.equals(Sign.PAPER)){
                    draw++;
                }
                break;
        }
    }

    public int getPlayerOneWins() {
        return playerOneWins;
    }

    public void setPlayerOneWins(int playerOneWins) {
        this.playerOneWins = playerOneWins;
    }

    public int getPlayerTwoWins() {
        return playerTwoWins;
    }

    public void setPlayerTwoWins(int playerTwoWins) {
        this.playerTwoWins = playerTwoWins;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getRounds() {
        return rounds;
    }

    public void setRounds(int rounds) {
        this.rounds = rounds;
    }

    public void setGameStatsZero(){
        this.draw=0;
        this.playerOneWins=0;
        this.playerTwoWins=0;
    }

}
