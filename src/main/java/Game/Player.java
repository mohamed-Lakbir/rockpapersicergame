package Game;
import Game.Interfaces.Hand;
public class Player {
    private Hand hand;
    public Player (String strategieName){
        chooseStrategie(strategieName);
    }
    public Sign playRockPaperSicer(){
        return  hand.decideSign();
    }
    public void chooseStrategie(String strategieName){
        strategieName = strategieName.toUpperCase();

        if(strategieName.equals("RANDOM")){
            hand = new RandomHandStrategie();
        }
        if(strategieName.equals("PAPER")){
            hand = new OnlyPaperHandStrategie();
        }
    }
    public Hand getHand() {
        return hand;
    }
    public void setHand(Hand hand) {
        this.hand = hand;
    }
}