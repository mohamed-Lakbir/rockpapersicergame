package Game.Interfaces;

import Game.Sign;

public interface Hand {

    Sign decideSign();
}
