package Game;

import Game.Interfaces.Hand;

public class OnlyPaperHandStrategie implements Hand {

    public Sign decideSign() {
        return Sign.PAPER;
    }
}
