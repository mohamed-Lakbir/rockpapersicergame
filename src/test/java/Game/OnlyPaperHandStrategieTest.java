package Game;
import Game.Interfaces.Hand;
import Game.OnlyPaperHandStrategie;
import Game.Sign;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
public class OnlyPaperHandStrategieTest {
    @Test
    public void decideSign() {
        Hand paperHand = new OnlyPaperHandStrategie();
        assertEquals(Sign.PAPER,paperHand.decideSign());
    }
}