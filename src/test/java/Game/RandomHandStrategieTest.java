package Game;
import Game.Interfaces.Hand;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class RandomHandStrategieTest {
    @Test
    public void decideSign() {
        int rockNumber=0;
        int paperNumber=0;
        int sicerNumber=0;
        Hand randomHand = new RandomHandStrategie();
        for(int i=0;i<12000000;i++){
            switch (randomHand.decideSign()){
                case ROCK:
                    rockNumber++;
                    break;
                case PAPER:
                    paperNumber++;
                    break;
                case SICER:
                    sicerNumber++;
                    break;
            }
        }
        assertTrue(10000<rockNumber&&rockNumber<10000000);
        assertTrue(10000<paperNumber&&paperNumber<10000000);
        assertTrue(10000<sicerNumber&&sicerNumber<10000000);
    }
}