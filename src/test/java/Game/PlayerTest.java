package Game;

import Game.OnlyPaperHandStrategie;
import Game.Player;
import Game.RandomHandStrategie;
import Game.Sign;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class PlayerTest {
    @Test
    public void playRockPaperSicer() {
        Player helperObject_one = new Player("Paper");
        Player helperObject_two = new Player("Random");

        assertTrue(helperObject_one.playRockPaperSicer() instanceof Sign);
        assertTrue(helperObject_two.playRockPaperSicer() instanceof Sign);
    }

    @Test
    public void chooseStrategie() {
        Player helperObject_one = new Player("Paper");
        Player helperObject_two = new Player("Random");

        assertTrue(helperObject_one.getHand() instanceof OnlyPaperHandStrategie);
        assertTrue(helperObject_two.getHand() instanceof RandomHandStrategie);

    }
}