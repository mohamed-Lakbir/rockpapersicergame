package Game;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class RockPaperSicerGameTest {



    @Test
    public void rule() {
        RockPaperSicerGame rockPaperSicerGameHelperObject = new RockPaperSicerGame();

        //Win Condition for Playerone test
        rockPaperSicerGameHelperObject.rule(Sign.PAPER,Sign.ROCK);
        assertTrue(rockPaperSicerGameHelperObject.getPlayerOneWins()==1);
        rockPaperSicerGameHelperObject.setGameStatsZero();

        rockPaperSicerGameHelperObject.rule(Sign.SICER,Sign.PAPER);
        assertTrue(rockPaperSicerGameHelperObject.getPlayerOneWins()==1);
        rockPaperSicerGameHelperObject.setGameStatsZero();

        rockPaperSicerGameHelperObject.rule(Sign.ROCK,Sign.SICER);
        assertTrue(rockPaperSicerGameHelperObject.getPlayerOneWins()==1);
        rockPaperSicerGameHelperObject.setGameStatsZero();


        //Win Condition for Playertwo test

        rockPaperSicerGameHelperObject.rule(Sign.ROCK,Sign.PAPER);
        assertTrue(rockPaperSicerGameHelperObject.getPlayerTwoWins()==1);
        rockPaperSicerGameHelperObject.setGameStatsZero();

        rockPaperSicerGameHelperObject.rule(Sign.PAPER,Sign.SICER);
        assertTrue(rockPaperSicerGameHelperObject.getPlayerTwoWins()==1);
        rockPaperSicerGameHelperObject.setGameStatsZero();

        rockPaperSicerGameHelperObject.rule(Sign.SICER,Sign.ROCK);
        assertTrue(rockPaperSicerGameHelperObject.getPlayerTwoWins()==1);
        rockPaperSicerGameHelperObject.setGameStatsZero();

        //Draw Condition test

        rockPaperSicerGameHelperObject.rule(Sign.PAPER,Sign.PAPER);
        assertTrue(rockPaperSicerGameHelperObject.getDraw()==1);
        rockPaperSicerGameHelperObject.setGameStatsZero();

        rockPaperSicerGameHelperObject.rule(Sign.SICER,Sign.SICER);
        assertTrue(rockPaperSicerGameHelperObject.getDraw()==1);
        rockPaperSicerGameHelperObject.setGameStatsZero();

        rockPaperSicerGameHelperObject.rule(Sign.ROCK,Sign.ROCK);
        assertTrue(rockPaperSicerGameHelperObject.getDraw()==1);
        rockPaperSicerGameHelperObject.setGameStatsZero();
    }
}